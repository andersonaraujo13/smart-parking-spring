Vue.component('button-counter', {
  data: function () {
    return {
      count: 0
    }
  },
  template: '<div class="alert alert-primary" role="alert">A simple primary alert—check it out!</div>'
})

Vue.component('alert-box', {
  template: `
    <div class="alert alert-primary">
      <strong>Erro!</strong>
      <slot></slot>
    </div>
  `
})

var app8 = new Vue({
	el : '#components-demo'
});

var app = new Vue({
	el: '#app',
	  data () {
	    return {
	      info: 'valor teste'
	    }
	  },
	  mounted () {
	    axios
	      .get('public/auth/teste')
	      .then(response => (this.info = response.data))
	  }
});

var app5 = new Vue({
	el : '#app-5',
	data : {
		message : 'Hello Vue.js!'
	},
	methods : {
		reverseMessage : function() {
			this.message = this.message.split('').reverse().join('')
		}
	}
})

var app2 = new Vue({
	el : '#app-2',
	data : {
		message : 'You loaded this page on ' + new Date().toLocaleString()
	}
})

var app3 = new Vue({
	el : '#app-3',
	data : {
		seen : false
	}
})

var app4 = new Vue({
	el : '#app-4',
	data : {
		todos : [ {
			text : 'Learn JavaScript'
		}, {
			text : 'Learn Vue'
		}, {
			text : 'Build something awesome'
		} ]
	}
})