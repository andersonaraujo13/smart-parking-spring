package br.com.ufrn.imd.smartparking.dominio;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pessoa", schema = "basico")
public class Pessoa implements Serializable{

	private static final long serialVersionUID = 7453337067719708356L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_vaga", unique = true, nullable = false)
	private int id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "nome_asc2")
	private String nomeAsc2;
	
	@Column(name = "cpf")
	private String cpf;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento")
	private Date dataNascimento;
	
	@Column(name = "deficiente")
	private boolean deficiente;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "pessoa")
	private List<Contatos> contatos;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public boolean isDeficiente() {
		return deficiente;
	}
	public void setDeficiente(boolean deficiente) {
		this.deficiente = deficiente;
	}
	public List<Contatos> getContatos() {
		return contatos;
	}
	public void setContatos(List<Contatos> contatos) {
		this.contatos = contatos;
	}
	public Date getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getNomeAsc2() {
		return nomeAsc2;
	}
	public void setNomeAsc2(String nomeAsc2) {
		this.nomeAsc2 = nomeAsc2;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
