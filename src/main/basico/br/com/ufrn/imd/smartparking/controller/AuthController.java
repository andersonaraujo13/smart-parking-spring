package br.com.ufrn.imd.smartparking.controller;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/public/auth")
public class AuthController  {

	@GetMapping(value = "/login" )
	public String login() {
		return "public/auth/login";
	}
	
	@PostMapping(value = "/login")
	public String login(String valor) {
		return "";
	}
	
	@GetMapping(value = "/teste", produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public String getTeste() {
		return "index";
	}
	
}
