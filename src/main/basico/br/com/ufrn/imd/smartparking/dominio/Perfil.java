package br.com.ufrn.imd.smartparking.dominio;

public enum Perfil {
	ADMINISTRADOR("ADMINISTRADOR"), NORMAL("NORMAL"), MOTORISTA("MOTORISTA"), SEM_PERFIL("SEM_PERFIL");
    
    private final String valor;
    
    Perfil(String valorOpcao){
        valor = valorOpcao;
    }
    
    public String getValor(){
        return valor;
    }
}
