package br.com.ufrn.imd.smartparking.dominio;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "historico_estacionamento", schema = "basico")
public class HistoricoEstacionamento implements Serializable {

	private static final long serialVersionUID = -6786447073025391695L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_vaga", unique = true, nullable = false)
	private int id;
	
	@Column(name = "data_entrada")
	private Date dataEntrada;
	
	@Column(name = "data_saida")
	private Date dataSaida;
	
	@OneToOne
	@JoinColumn(name = "id_vaga")
	private Vaga vaga;
	
	@OneToOne
	@JoinColumn(name = "id_motorista")
	private Motorista motorista;
	
	public Motorista getMotorista() {
		return motorista;
	}
	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}
	public Date getDataEntrada() {
		return dataEntrada;
	}
	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	public Date getDataSaida() {
		return dataSaida;
	}
	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Vaga getVaga() {
		return vaga;
	}
	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}
	
}
