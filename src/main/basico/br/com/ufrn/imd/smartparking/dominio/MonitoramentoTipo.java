package br.com.ufrn.imd.smartparking.dominio;

public enum MonitoramentoTipo {
	CAMERA("CAMERA"), SENSOR_PRESENCA("SENSOR_PRESENCA"), SENSOR_DISTANCIA("SENSOR_DISTANCIA"), SENSOR_INFRAVERMELHO("SENSOR_INFRAVERMELHO");
    
    private final String valor;
    
    MonitoramentoTipo(String valorOpcao){
        valor = valorOpcao;
    }
    
    public String getValor(){
        return valor;
    }
}
