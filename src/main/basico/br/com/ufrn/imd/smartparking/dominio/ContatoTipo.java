package br.com.ufrn.imd.smartparking.dominio;

public enum ContatoTipo {
	CELULAR("CELULAR"), FIXO("FIXO"), EMAIL("EMAIL"), SITE("SITE");
    
    private final String valor;
    
    ContatoTipo(String valorOpcao){
        valor = valorOpcao;
    }
    
    public String getValor(){
        return valor;
    }
}
